#include <cstdio>
#include <cstring>
#include <iostream>
using namespace std;

int main()
{
    int c, n, i, ck=0;
    char s='%';
    double ans, sum=0, avg;
    scanf("%d", &c);

    for(int j=1;j<=c;++j){
        scanf("%d", &n);
        int arr[n];
        for(i=0;i<n;++i){
            scanf("%d", &arr[i]);
            sum+=arr[i];
        }
        avg = sum/n;
        for(i=0;i<n;++i){
           if(arr[i]>avg){
            ck++;
           }
        }
        ans = (double)ck/(double)n;
        printf("%.3f%c\n", ans*100, s);
        ck=0;
        sum=0;
    }

    return 0;
}
